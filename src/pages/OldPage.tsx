import { IonContent,
    IonSegment,
    IonSegmentButton, 
    IonLabel,
    IonGrid } from '@ionic/react';
import React, { useState } from 'react';
import BookList from '../components/BookList';
import Term from '../components/Term';
  
import './Home.css';
  
const OldPage: React.FC<any> = (props) => {

    const [page, setPage] = useState<'term' | 'chapter'>('term');

    let user_id = 2;

    return(
        <IonContent className="book-page">
          <IonSegment className="segment-btn" color="dark" onIonChange={e => setPage(e.detail.value as any)} value={page}>
            <IonSegmentButton value="term">
              <IonLabel>Term</IonLabel>
            </IonSegmentButton>
            <IonSegmentButton value="chapter">
              <IonLabel>Chapter</IonLabel>
            </IonSegmentButton>
          </IonSegment>
          <IonGrid>
            {page == 'term' ? <Term/> :  <BookList user={user_id}/>}
          </IonGrid>
        </IonContent>
    )
}

export default OldPage;