import { IonPage,
    IonToolbar,
    IonHeader,
    IonTitle,
    IonContent } from '@ionic/react';
import React from 'react';
  
import './Home.css';
  
const Analytics: React.FC<any> = (props) => {

    return(
    <IonPage>
        <IonHeader>
            <IonToolbar>
                <IonTitle>Analytics</IonTitle>
            </IonToolbar>
        </IonHeader>
        <IonContent className="book-page">

        </IonContent>
    </IonPage>
    )
}

export default Analytics;