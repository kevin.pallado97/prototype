import React, { useState } from 'react';

import { IonRow, 
    IonSearchbar } from '@ionic/react';
const Search: React.FC<any> = (props) => {

    return(
        <>
            <IonRow>
                <IonSearchbar showCancelButton="focus"></IonSearchbar>
            </IonRow>
        </>   
    )
}

export default Search;