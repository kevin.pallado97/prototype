import { IonPage,
    IonToolbar,
    IonHeader,
    IonTitle,
    IonContent,
    IonSegment,
    IonSegmentButton, 
    IonLabel,
    IonGrid } from '@ionic/react';
import React, { useState } from 'react';

/** Components */
import BookList from '../components/BookList';
import Term from '../components/Term';

/** Styles */
import './Home.css';
  
const Clips: React.FC<any> = (props) => {

    const [page, setPage] = useState<'term' | 'chapter'>('term');

    return(
    <IonPage>
        <IonHeader>
            <IonToolbar>
                <IonTitle>Book</IonTitle>
            </IonToolbar>
        </IonHeader>
        <IonContent className="book-page">
            {/* <BookList user={user_id}/> */}
            <IonSegment className="segment-btn" color="dark" onIonChange={e => setPage(e.detail.value as any)} value={page}>
                <IonSegmentButton value="term">
                <IonLabel>Term</IonLabel>
                </IonSegmentButton>
                <IonSegmentButton value="chapter">
                <IonLabel>Chapter</IonLabel>
                </IonSegmentButton>
            </IonSegment>
            <IonGrid>
                {page == 'term' ? <Term/> :  <BookList/>}
            </IonGrid>
        </IonContent>
    </IonPage>
    )
}

export default Clips;