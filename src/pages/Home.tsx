import { useIonViewWillEnter,
  IonLabel,
  IonRouterOutlet,
  IonTabs, 
  IonTabBar, 
  IonTabButton, 
  IonIcon } from '@ionic/react';
import React, { useState } from 'react';
import { Route, Redirect } from 'react-router';

import Clips from './Clips';
import Term from './Profile';
import Analytics from './Analytics';

import { analyticsOutline, personOutline, bookOutline } from 'ionicons/icons';

import './Home.css';

const Home: React.FC = () => {

  return (
      <IonTabs>
        <IonRouterOutlet>
          <Redirect exact path="/tabs" to="/tabs/clips" />
          <Route path="/tabs/clips" render={() => <Clips/>} exact={true} />
          <Route path="/tabs/analytics" render={() => <Analytics/> } exact={true} />
          <Route path="/tabs/profile" render={() => <Term/> } exact={true} />
        </IonRouterOutlet>
        <IonTabBar color="tab-bar" slot="bottom">
          <IonTabButton tab="clips" href="/tabs/clips">
            <IonIcon icon={bookOutline} />
              <IonLabel>Clips</IonLabel>
          </IonTabButton>
        
          <IonTabButton tab="analytics" href="/tabs/analytics">
            <IonIcon icon={analyticsOutline} />
              <IonLabel>Analytics</IonLabel>
          </IonTabButton>
        
          <IonTabButton tab="profile" href="/tabs/profile">
            <IonIcon icon={personOutline} />
              <IonLabel>Profile</IonLabel>
          </IonTabButton>
        </IonTabBar>
      </IonTabs>
  );
};

export default Home;
