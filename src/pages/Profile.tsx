import { IonPage,
    IonAvatar,
    IonRow,
    IonCol,
    IonImg,
    IonCard,
    IonCardHeader,
    IonCardContent,
    IonCardSubtitle,
    IonAlert,
    IonToolbar,
    IonButtons,
    IonButton,
    IonIcon,
    useIonViewWillEnter,
    IonLabel,
    IonItem,
    IonModal,
    IonTextarea,
    IonRange,
    IonTitle,
    IonContent, 
    IonHeader,
    IonSelect,
    IonSelectOption,
    IonFooter} from '@ionic/react';

import React, { useState } from 'react';
import { mailOutline, helpCircle, bookOutline, menuOutline } from 'ionicons/icons';

import Help from '../components/Help';

import './Profile.css';
  
const Term: React.FC<any> = (props) => {
    const [genderAlert, setGender] = useState(false);
    const [languageAlert, setAlertLanguage] = useState(false);
    const [language, setLanguage] = useState<'English' | 'British English' | 'Spanish' | 'Portuguese'>('Spanish');
    const [width, setWidth] = useState(0);
    const [showModal, setShowModal] = useState(false);
    const [value, setValue] = useState(0);
    const [text, setText] = useState<string>();
    const [selectedUsers, setSelectedUsers] = useState('Real Estate');

    const study_topic = [
        {
          id: 1,
          topic: 'Real Estate'
        },
        {
          id: 2,
          topic: 'CPA exam 1',
        },
        {
          id: 3,
          topic: 'Nurse level 1',
        }
    ];

    useIonViewWillEnter(() => {
        setWidth(window.innerWidth)
    });

    return(
    <IonPage id="main">
        <IonContent className="book-page">
            <IonToolbar className="profile-toolbar">
                <IonButtons slot="start">
                    <IonButton onClick={() => setShowModal(true)} fill="clear">
                        <IonIcon slot="start" icon={mailOutline} /><p className="toolbar-btn-title">Send Feedback</p>
                    </IonButton>
                </IonButtons>
                <IonButtons slot="end">
                    
                    <IonButton onClick={() => setShowModal(true)} fill="clear">
                        <p className="toolbar-btn-title">Get Help</p><IonIcon slot="end" icon={helpCircle} />
                    </IonButton>
                </IonButtons>
            </IonToolbar>
            <IonModal isOpen={showModal} cssClass='my-custom-class'>
                <IonHeader>
                    <IonToolbar className="book-page">
                        <IonTitle>Note</IonTitle>
                    </IonToolbar>
                </IonHeader>
                <IonContent>
                    <IonRow>
                        <IonCol className="help-modal">
                            <IonItem className="ion-margin">
                                <IonTextarea rows={6} cols={20} placeholder="Enter any notes" value={text} onIonChange={e => setText(e.detail.value!)}></IonTextarea>
                            </IonItem>
                        </IonCol>
                    </IonRow>
                    {/* <IonRow>
                        <IonCol className="ion-text-right" size="12">
                            <IonButton onClick={()=> { setShowModal(false) }} color="success">Submit</IonButton>
                        </IonCol>
                    </IonRow> */}
                </IonContent>
                <IonFooter>
                    <IonRow>
                        <IonCol size="6">
                            <IonButton className="modal-footer-button" expand="full" onClick={()=> { setShowModal(false) }} color="success">Close</IonButton>
                        </IonCol>
                        <IonCol size="6">
                            <IonButton className="modal-footer-button" expand="full" onClick={()=> { setShowModal(false) }} color="success">Submit</IonButton>
                        </IonCol>
                    </IonRow>
                </IonFooter>
            </IonModal>
            <IonAlert
                isOpen={languageAlert}
                onDidDismiss={() => setAlertLanguage(false)}
                cssClass='my-custom-class'
                header={'Language'}
                inputs={[
                    {
                    name: 'english',
                    type: 'radio',
                    label: 'English',
                    value: 'English',
                    checked: language == 'English' ? true : false
                    },
                    {
                    name: 'british',
                    type: 'radio',
                    label: 'British English',
                    value: 'British English',
                    checked: language == 'British English' ? true : false
                    },
                    {
                    name: 'spanish',
                    type: 'radio',
                    label: 'Spanish',
                    value: 'Spanish',
                    checked: language == 'Spanish' ? true : false
                    },
                    {
                    name: 'portuguese',
                    type: 'radio',
                    label: 'Portuguese',
                    value: 'Portuguese',
                    checked: language == 'Portuguese' ? true : false
                    },
                ]}
                buttons={[
                    {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                        console.log('Confirm Cancel');
                    }
                    },
                    {
                    text: 'Ok',
                    handler: (e) => {
                        setLanguage(e);
                    }
                    }
                ]}
            />
            <IonAlert
                isOpen={genderAlert}
                onDidDismiss={() => setGender(false)}
                cssClass='my-custom-class'
                header={'Gender'}
                inputs={[
                    {
                    name: 'male',
                    type: 'radio',
                    label: 'Male',
                    value: 'male',
                    checked: true
                    },
                    {
                    name: 'female',
                    type: 'radio',
                    label: 'Female',
                    value: 'female'
                    },
                ]}
                buttons={[
                    {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                        console.log('Confirm Cancel');
                    }
                    },
                    {
                    text: 'Ok',
                    handler: () => {
                        console.log('Confirm Ok');
                    }
                    }
                ]}
            />
            <IonAvatar className="profile-avatar">
                <img src="https://gravatar.com/avatar/dba6bae8c566f9d4041fb9cd9ada7741?d=identicon&f=y" />
            </IonAvatar>
            <h4 className="profile-name ion-text-center">
                John Doe <IonButton color="danger" shape="round" size="small">Change User?</IonButton>
            </h4>
            <p className="ion-text-center">34hjd7@betalex.ai  |  value@gmail.com</p>
            <IonRow>
                <IonCol size="12">
                    <IonCard>
                        <IonCardContent>
                            <div className="study-topic">
                            <IonItem>
                                <IonLabel>Study Topic</IonLabel>
                                <IonSelect value={selectedUsers} onIonChange={e => setSelectedUsers(e.detail.value)}>
                                    {study_topic.map(element => (
                                        <IonSelectOption key={element.id} value={element.topic}>
                                        {element.topic}
                                        </IonSelectOption>
                                    ))}
                                </IonSelect>
                            </IonItem>
                            </div>
                            <div className="read-speed">
                                <IonLabel>Read-back Speed: <span>{value}</span></IonLabel>
                                <IonRange  min={0.75} max={1.50} step={0.25} pin={true} value={value} onIonChange={e => setValue(e.detail.value as number)} >
                                    <IonIcon size="small" slot="start" icon={bookOutline} />
                                    <IonIcon slot="end" icon={bookOutline} />
                                </IonRange>
                            </div>
                        </IonCardContent>
                    </IonCard>
                </IonCol>
                <IonCol className="ion-text-center" size="6">
                    <IonCard>
                        <IonCardHeader>
                            <IonCardSubtitle>Spoken Language for Highlights</IonCardSubtitle>
                        </IonCardHeader>
                        <IonCardContent>
                            <IonRow>
                                <IonCol size="6" onClick={()=> { setAlertLanguage(true)}}>
                                    <IonImg className="context-image" src="assets/img/conversation.png" />
                                    <p className="context-result">{language}</p>
                                </IonCol>
                                <IonCol size="6" onClick={()=> { setGender(true)}}>
                                    <IonImg className="context-image" src="assets/img/male.png" />
                                    <p className="context-result">Male</p>
                                </IonCol>
                            </IonRow>
                        </IonCardContent>
                    </IonCard>
                </IonCol>
                <IonCol className="ion-text-center" size="6">
                    <IonCard>
                        <IonCardHeader>
                            <IonCardSubtitle>Spoken Language for Flash Cards</IonCardSubtitle>
                        </IonCardHeader>
                        <IonCardContent>
                            <IonRow>
                                <IonCol size="6" onClick={()=> { setAlertLanguage(true)}}>
                                    <IonImg className="context-image" src="assets/img/conversation.png" />
                                    <p className="context-result">{language}</p>
                                </IonCol>
                                <IonCol size="6" onClick={()=> { setGender(true)}}>
                                    <IonImg className="context-image" src="assets/img/female.png" />
                                    <p className="context-result">Female</p>
                                </IonCol>
                            </IonRow>
                        </IonCardContent>
                    </IonCard>
                </IonCol>
            </IonRow>
            <IonRow>
                <IonCol className="ion-text-center" size="12">
                    <IonCard>
                        <IonCardHeader>
                            <IonCardSubtitle>Data Management</IonCardSubtitle>
                        </IonCardHeader>
                        <IonCardContent>
                            <IonRow>
                                <IonCol size="6">
                                    <IonImg className="data-management-image" src="assets/img/study.png" />
                                    <p className="context-result">Archive Study Topic</p>
                                </IonCol>
                                <IonCol size="6">
                                    <IonImg className="data-management-image" src="assets/img/archive.png" />
                                    <p className="context-result">Archive Book Topic</p>
                                </IonCol>
                            </IonRow>
                            <IonRow>
                                <IonCol size="6">
                                    <IonImg className="data-management-image" src="assets/img/glosarry.png" />
                                    <p className="context-result">Add Glosarry for Book</p>
                                </IonCol>
                                <IonCol size="6">
                                    <IonImg className="data-management-image" src="assets/img/topic.png" />
                                    <p className="context-result">Add Study Topic Area</p>
                                </IonCol>
                            </IonRow>
                            <IonRow>
                                <IonCol size="12">
                                    <IonImg className="data-management-image" src="assets/img/storage.png" />
                                    <p className="context-result">Adjust local storage options</p>
                                </IonCol>
                            </IonRow>
                        </IonCardContent>
                    </IonCard>
                </IonCol>
            </IonRow>
        </IonContent>
    </IonPage>
    )
}

export default Term;