import React from "react";

const UserContext = React.createContext({
    book: '',
    total_chapters: 0,
    book_id: 0,
    chapters: ['']
});
export default UserContext;