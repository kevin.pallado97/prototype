import React, { useState } from 'react';

import { IonCard, IonRow, IonCardContent, IonSearchbar, IonButton, IonCol,IonHeader, IonModal, IonContent, IonToolbar, IonTitle } from '@ionic/react';

import ModalTerm from './ModalTerm';

import './Term.css'

const Term: React.FC<any> = (props) => {
    const modalInformation = {
        id : 0,
        show : true
    }

    const [searchText, setSearchText] = useState('');
    const [showModal, setShowModal] = useState(false);
    const [modalContext, setModalContext] = useState(modalInformation);

    const showModalTerm = () => {
        setShowModal(true);
        setModalContext({
            id: 1,
            show: true
        });
    }
    const dismissModal = () => {
        setShowModal(false);
    }
    return(
        <>
            <IonRow>
                <IonSearchbar value={searchText} onIonChange={e => setSearchText(e.detail.value!)} showCancelButton="focus"></IonSearchbar>
            </IonRow>
            <IonRow>
                <div className="ion-text-center card-title"><p>Most Popular</p></div>
                <IonCard color="list" className="card">
                    <IonCardContent>
                        <ul className="card-list">
                            <li onClick={()=> { showModalTerm() }}><u>null</u></li>
                            <li onClick={()=> { showModalTerm() }}><u>divide by zero</u></li>
                            <li onClick={()=> { showModalTerm() }}><u>black holes</u></li>
                        </ul>
                    </IonCardContent>
                </IonCard>
            </IonRow>
            <IonRow>
                <div className="ion-text-center card-title">Most Recent</div>
                <IonCard color="list" className="card">
                    <IonCardContent>
                        <ul className="card-list">
                            <li onClick={()=> { showModalTerm() }}><u>vectors</u></li>
                            <li onClick={()=> { showModalTerm() }}><u>Frozen Fractals</u></li>
                            <li onClick={()=> { showModalTerm() }}><u>circles</u></li>
                        </ul>
                    </IonCardContent>
                </IonCard>
            </IonRow>
            <IonRow>
                <IonCol className="ion-text-center" size="6">
                    <IonButton className="term-button" color="books" shape="round">Speak All</IonButton>    
                </IonCol>
                <IonCol className="ion-text-center" size="6">
                    <IonButton className="term-button" color="books" shape="round">Flash Cards</IonButton>
                </IonCol>
            </IonRow>
            <IonModal isOpen={showModal} cssClass='my-custom-class'>
                <IonHeader translucent>
                    <IonToolbar>
                        <IonTitle>Term Content</IonTitle>
                    <IonButton slot="end">
                        <p onClick={()=> dismissModal()}>Close</p>
                    </IonButton>
                    </IonToolbar>
                </IonHeader>
                <IonContent>
                    <ModalTerm context={modalContext}/>
                </IonContent>
            </IonModal>
        </>
    )
}

export default Term;