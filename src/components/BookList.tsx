import React, { useState } from 'react';

import Collapsible from 'react-collapsible';

import ChapterList from '../components/ChapterList';

import '../pages/Home.css';

import { IonSearchbar,
    IonRow,
    IonCol,
    IonCard,
    IonCardContent,
    IonAlert,
    IonButton } from '@ionic/react';


const BookList: React.FC<any> = (props) => {

    const [searchText, setSearchText] = useState('');
    const [showSpoken, setSpoken] = useState(false);
    const [showCards, setCards] = useState(false);
    const [showAlert3, setShowAlert3] = useState(false);
    const [showView, setView] = useState<'book' | 'chapter'>('book');
    
    const chapter = [
        {
            "uid": 1,
            "book_id" : 2,
            "id" : 1,
            "segment" : "Count to Ten",
            "content" : "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            "segment_chapter": 5,
            "segment_number" : 6,
            "terms" : []
        },
        {
            "uid": 2,
            "book_id" : 2,
            "id" : 2,
            "segment" : "Small Worlds and Large Worlds",
            "content" : "true definition small. It’s like saying that the probability of rain and cold on the same day is equal to the probability that it’s cold, when it’s raining, times the probability of rain. Compare this statement to the one in the previous paragraph. Now since both right-hand sides above are equal to the same thing, Pr(W, L, p), they are also equal to one another: Pr(W, L|p) Pr(p) = Pr(p|W, L) Pr(W, L) So we can now solve for the thing that we want, Pr(p|W, L): Pr(p|W, L) = Pr(W, L|p) Pr(p) Pr(W, L) And this is Bayes’ theorem. It says that the probability of any particular value of p, considering the data, is equal to the product of the relative plausibility of the data, conditional on p, and the prior plausibility of p, divided by this thing Pr(W, L), which I’ll call the average probability of the data. In word form: Posterior = Probability of the data × Prior Average probability of the data The average probability of the data, Pr(W, L), can be confusing. It is commonly called the “evidence” or the “average likelihood,” neither of which is a transparent name. The probability Pr(W, L)is literally the average probability of the data. Averaged over what? Averaged over the prior. It’s job is just to standardize the posterior, to ensure it sums (integrates) to one. In mathematical form: Pr(W, L) = E Pr(W, L|p)   = Z Pr(W, L|p) Pr(p)dp The operator E means to take an expectation. Such averages are commonly called marginals in mathematical statistics, and so you may also see this same probability called a marginal likelihood. And the integral above just defines the proper way to compute the average over a continuous distribution of values, like the infinite possible values of p. The key lesson is that the posterior is proportional to the product of the prior and the probability of the data. Why? Because for each specific value of p, the number of paths through the garden of forking data is the product of the prior number of paths and the new number of paths. Multiplication is just compressed counting. The average probability on the bottom just standardizes the counts so they sum to one. So while Bayes’ theorem looks complicated, because the relationship with counting paths is obscured,",
            "terms" : ["probability","plausibility","transparent name", "compressed counting"]
        },
        {
            "uid": 3,
            "book_id" : 2,
            "id" : 3,
            "segment" : "Sampling the Imaginary",
            "content" : "scenario. To repeat the structure of common examples, suppose there is a blood test that correctly detects vampirism 95% of the time. In more precise and mathematical notation, Pr(positive test result|vampire) = 0.95. It’s a very accurate test, nearly always catching real vampires. It also make mistakes, though, in the form of false positives. One percent of the time, it incorrectly diagnoses normal people as vampires, Pr(positive test result|mortal) = 0.01. The final bit of information we are told is that vampires are rather rare, being only 0.1% of the population, implying Pr(vampire) = 0.001. Suppose now that someone tests positive for vampirism. What’s the probability that he or she is a bloodsucking immortal? The correct approach is just to use Bayes’ theorem to invert the probability, to compute Pr(vampire|positive). The calculation can be presented as: Pr(vampire|positive) = Pr(positive|vampire) Pr(vampire) Pr(positive) where Pr(positive) is the average probability of a positive test result, that is, Pr(positive) = Pr(positive|vampire) Pr(vampire) + Pr(positive|mortal) 1 − Pr(vampire) Overthinking: Counting with sum. In the R code examples just above, I used the function sum to effectively count up how many samples fulfill a logical criterion. Why does this work? It works because R internally converts a logical expression, like samples < 0.5, to a vector of TRUE and FALSE results, one for each element of samples, saying whether or not each element matches the criterion. Go ahead and enter samples < 0.5 on the R prompt, to see this for yourself. Then when you sum this vector of TRUE and FALSE, R counts each TRUE as 1 and each FALSE as 0. So it ends up counting how many TRUE values are in the vector, which is the same as the number of elements in samples that match the logical criterion. 3.2.2. Intervals of defined",
            "terms" : ["probability", "false positives","vector"]
        },
        {
            "uid": 4,
            "book_id" : 2,
            "id" : 4,
            "segment" : "Count to infinity",
            "content" : "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?",
            "segment_chapter": 8,
            "segment_number" : 6,
            "terms" : []
        },
        {
            "uid": 5,
            "book_id" : 3,
            "id" : 1,
            "segment" : "Count to Ten",
            "content" : "In ultricies ac nisl ut suscipit. Sed accumsan neque eu turpis tincidunt, nec tincidunt velit maximus. Aenean dignissim lorem vitae vehicula molestie. Suspendisse sed mi semper, aliquam mauris ac, scelerisque ligula. Curabitur iaculis quis neque at bibendum. Etiam a ipsum vel neque dictum finibus. Fusce sed lorem sit amet velit vestibulum varius. Phasellus eu magna mauris. Etiam ac sapien sit amet purus accumsan faucibus sed vitae risus. Integer lacinia nisi quis sem cursus tempor.",
            "segment_chapter": 5,
            "segment_number" : 14,
            "terms" : []
        },
        {
            "uid": 6,
            "book_id" : 3,
            "id" : 2,
            "segment" : "Count to infinity",
            "content" : "Phasellus pellentesque dictum sapien at blandit. Pellentesque efficitur arcu odio, in placerat lacus bibendum sit amet. Sed finibus tellus et augue fringilla cursus. Phasellus dapibus velit gravida, maximus turpis eget, condimentum nisl. Curabitur vehicula accumsan finibus. Duis vitae dolor posuere, egestas dolor id, porttitor urna. Aenean dictum quam tincidunt elit faucibus condimentum. Nulla vel massa in elit suscipit lacinia ut ac libero. Aliquam aliquam, sem nec tempor consectetur, neque metus facilisis ex, non tempor velit elit eget mauris. Donec hendrerit ligula sed nisi molestie, at eleifend libero suscipit. Morbi sed massa ut orci sollicitudin placerat. Nunc aliquam felis quis ligula accumsan, quis fermentum mi consectetur.",
            "segment_chapter": 8,
            "segment_number" : 20,
            "terms" : []
        },
        {
            "uid": 7,
            "book_id" : 4,
            "id" : 1,
            "segment" : "Count to Ten",
            "content" : "Phasellus varius dui nibh, a mollis mauris mattis eu. Vivamus pretium, diam ac bibendum tristique, elit libero scelerisque dolor, quis vestibulum augue massa non augue. Mauris vel interdum tellus. Phasellus at urna pulvinar, ultricies purus in, pellentesque nisi. Aenean tincidunt vehicula venenatis. Aenean laoreet vehicula dapibus. Duis id ante quis est sollicitudin dignissim eget vitae eros. Proin interdum augue sed justo sollicitudin sollicitudin.",
            "segment_chapter": 5,
            "segment_number" : 26,
            "terms" : []
        },
        {
            "uid": 8,
            "book_id" : 4,
            "id" : 2,
            "segment" : "Count to infinity",
            "content" : "Vivamus blandit rutrum tincidunt. Morbi id diam ut enim tincidunt iaculis. Nullam vestibulum posuere vulputate. Sed sed scelerisque elit. Quisque vehicula augue ut felis ullamcorper laoreet. Nam orci ex, laoreet et mauris eu, interdum dapibus lectus. Donec ornare quam eget mattis tincidunt. Etiam fringilla interdum nulla, accumsan consectetur elit facilisis tincidunt. Curabitur facilisis lacus ac turpis finibus rutrum.",
            "segment_chapter": 8,
            "segment_number" : 30,
            "terms" : []
        }
    ]

    

    const data = [
        {
          "book" : "Know the Numbers",
          "total_chapters": 12,
          "book_id": 2,
          "chapters" : [
              {
                  "id" : 1,
                  "segment" : "Count to Ten",
                  "content" : "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                  "segment_chapter": 5,
                  "segment_number" : 6
              },
              {
                  "id" : 2,
                  "segment" : "Small Worlds and Large Worlds",
                  "content" : "true definition small. It’s like saying that the probability of rain and cold on the same day is equal to the probability that it’s cold, when it’s raining, times the probability of rain. Compare this statement to the one in the previous paragraph. Now since both right-hand sides above are equal to the same thing, Pr(W, L, p), they are also equal to one another: Pr(W, L|p) Pr(p) = Pr(p|W, L) Pr(W, L) So we can now solve for the thing that we want, Pr(p|W, L): Pr(p|W, L) = Pr(W, L|p) Pr(p) Pr(W, L) And this is Bayes’ theorem. It says that the probability of any particular value of p, considering the data, is equal to the product of the relative plausibility of the data, conditional on p, and the prior plausibility of p, divided by this thing Pr(W, L), which I’ll call the average probability of the data. In word form: Posterior = Probability of the data × Prior Average probability of the data The average probability of the data, Pr(W, L), can be confusing. It is commonly called the “evidence” or the “average likelihood,” neither of which is a transparent name. The probability Pr(W, L)is literally the average probability of the data. Averaged over what? Averaged over the prior. It’s job is just to standardize the posterior, to ensure it sums (integrates) to one. In mathematical form: Pr(W, L) = E Pr(W, L|p)   = Z Pr(W, L|p) Pr(p)dp The operator E means to take an expectation. Such averages are commonly called marginals in mathematical statistics, and so you may also see this same probability called a marginal likelihood. And the integral above just defines the proper way to compute the average over a continuous distribution of values, like the infinite possible values of p. The key lesson is that the posterior is proportional to the product of the prior and the probability of the data. Why? Because for each specific value of p, the number of paths through the garden of forking data is the product of the prior number of paths and the new number of paths. Multiplication is just compressed counting. The average probability on the bottom just standardizes the counts so they sum to one. So while Bayes’ theorem looks complicated, because the relationship with counting paths is obscured,"
              },
              {
                  "id" : 3,
                  "segment" : "Sampling the Imaginary",
                  "content" : "scenario. To repeat the structure of common examples, suppose there is a blood test that correctly detects vampirism 95% of the time. In more precise and mathematical notation, Pr(positive test result|vampire) = 0.95. It’s a very accurate test, nearly always catching real vampires. It also make mistakes, though, in the form of false positives. One percent of the time, it incorrectly diagnoses normal people as vampires, Pr(positive test result|mortal) = 0.01. The final bit of information we are told is that vampires are rather rare, being only 0.1% of the population, implying Pr(vampire) = 0.001. Suppose now that someone tests positive for vampirism. What’s the probability that he or she is a bloodsucking immortal? The correct approach is just to use Bayes’ theorem to invert the probability, to compute Pr(vampire|positive). The calculation can be presented as: Pr(vampire|positive) = Pr(positive|vampire) Pr(vampire) Pr(positive) where Pr(positive) is the average probability of a positive test result, that is, Pr(positive) = Pr(positive|vampire) Pr(vampire) + Pr(positive|mortal) 1 − Pr(vampire) Overthinking: Counting with sum. In the R code examples just above, I used the function sum to effectively count up how many samples fulfill a logical criterion. Why does this work? It works because R internally converts a logical expression, like samples < 0.5, to a vector of TRUE and FALSE results, one for each element of samples, saying whether or not each element matches the criterion. Go ahead and enter samples < 0.5 on the R prompt, to see this for yourself. Then when you sum this vector of TRUE and FALSE, R counts each TRUE as 1 and each FALSE as 0. So it ends up counting how many TRUE values are in the vector, which is the same as the number of elements in samples that match the logical criterion. 3.2.2. Intervals of defined"
              },
              {
                  "id" : 4,
                  "segment" : "Count to infinity",
                  "content" : "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?",
                  "segment_chapter": 8,
                  "segment_number" : 6
              }
          ]
        },
        {
            "book" : "No Numbers Allowed",
            "total_chapters": 34,
            "book_id": 3,
            "chapters" : [
                {
                    "id" : 1,
                    "segment" : "Count to Ten",
                    "content" : "Sed eget pulvinar lacus. Nullam sagittis tincidunt nisi, at pulvinar felis porta vel. Proin suscipit dignissim mi, vel elementum eros finibus rhoncus. Mauris ornare tortor vitae nibh vestibulum vehicula. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aenean congue ligula vel massa varius, eget semper arcu finibus. Aliquam suscipit ac nisi a hendrerit. In sagittis volutpat accumsan. In finibus rhoncus turpis quis ultricies.",
                    "segment_chapter": 5,
                    "segment_number" : 14
                },
                {
                    "id" : 2,
                    "segment" : "Count to infinity",
                    "content" : "Donec elementum nunc id enim aliquet auctor. Mauris viverra lacus eu fermentum egestas. Morbi mauris nibh, porttitor quis odio at, interdum iaculis lectus. Donec sit amet mattis odio. In hac habitasse platea dictumst. Praesent consectetur malesuada nibh. Pellentesque tristique nulla ac tempus tempus.",
                    "segment_chapter": 8,
                    "segment_number" : 20
                }
            ]
        },
        {
            "book" : "All the number minus 5",
            "total_chapters": 56,
            "book_id": 4,
            "chapters" : [
                {
                    "id" : 1,
                    "segment" : "Count to Ten",
                    "content" : "Vestibulum rutrum placerat lacinia. Curabitur sagittis nunc in quam placerat vestibulum. Nullam sit amet ante semper, consequat sem in, iaculis nibh. Ut nisi dolor, sollicitudin et lacus ut, tincidunt pretium leo. Nulla vel varius lorem. Mauris feugiat ultricies turpis, non ultricies metus. Morbi faucibus enim tincidunt dui blandit tempor. In rhoncus luctus sem, nec commodo leo sagittis sed. Fusce euismod tempus diam a bibendum. Maecenas vitae lacus in neque porta imperdiet. Fusce laoreet, odio sed mollis finibus, massa nisi auctor nulla, ut viverra dui odio eget ligula. Duis pulvinar dui leo, sed gravida ligula gravida at. Mauris ultricies, lacus sit amet rhoncus fermentum, urna purus tincidunt neque, eget interdum sem velit non erat. Ut quam tortor, pulvinar ut posuere ac, consequat sed arcu. Nam venenatis arcu lacinia, malesuada tortor at, facilisis massa. Etiam luctus varius dictum.",
                    "segment_chapter": 5,
                    "segment_number" : 26
                },
                {
                    "id" : 2,
                    "segment" : "Count to infinity",
                    "content" : "Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris quis malesuada felis. Suspendisse eu mi mauris. Donec molestie varius finibus. Curabitur in ipsum mi. Mauris vel quam accumsan, bibendum leo at, scelerisque turpis. Aliquam arcu metus, ullamcorper ut lorem at, auctor dictum quam. Suspendisse eget orci sit amet lorem aliquet sodales. Cras pharetra in est ac interdum. Pellentesque vel purus aliquam, gravida leo et, consectetur odio. Vestibulum nunc augue, mattis at lacus viverra, cursus tempor magna. In sodales elementum feugiat. Quisque odio libero, feugiat ac libero id, lobortis ultricies magna.",
                    "segment_chapter": 8,
                    "segment_number" : 30
                }
            ]
        }
      ];
    
    
    const showChapter = (id : any) => {
        setSpoken(!showSpoken);
    }

    let lists = data.filter(
        (books) => {
            let fBook = books.book.toLowerCase().indexOf(searchText)
            return fBook !== -1
    })

    let chapterLists = chapter.filter(
        (chapters) => {
            let fChapter = chapters.content.toLowerCase().indexOf(searchText)
            return fChapter !== -1
    })
    
    const searchResults = (event: any) => {
        var sWord = event.target.value.toLowerCase();
        setSearchText(sWord);
        setSpoken(false);
        if(document.getElementsByClassName('chapter-content').length > 0) {
            setView('chapter')
            var hContent = document.getElementsByClassName('chapter-content');
            var hWords = hContent[0].innerHTML
            hWords = hWords.replace(/<\/?span[^>]*>/g,"")
            var index = hWords.toLowerCase().indexOf(sWord)
            if(index) {
                hWords = hWords.substring(0,index) + "<span class='highlight'>" + hWords.substring(index,index+sWord.length) + "</span>" + hWords.substring(index + sWord.length);
                hContent[0].innerHTML = hWords;
            }
        } else {
            setView('book')
            setCards(false)
        }
    }

    function createMarkup(text: any) {
        return {__html: text};
    }
    
    function randomColor() {
        var colors = ["dark","secondary","tertiary","success","warning","danger","light","books","list"]
        return colors[Math.floor(Math.random() * 8)]
    }

    return(
        <>
            <IonAlert
            isOpen={showAlert3}
            onDidDismiss={() => setShowAlert3(false)}
            cssClass='my-custom-class'
            header={'Confirm!'}
            message={'Message <strong>text</strong>!!!'}
            buttons={[
                {
                text: 'Cancel',
                role: 'cancel',
                cssClass: 'secondary',
                handler: blah => {
                    console.log('Confirm Cancel: blah');
                }
                },
                {
                text: 'Okay',
                handler: () => {
                    console.log('Confirm Okay');
                }
                }
            ]}
            />
            <IonRow>
                <IonSearchbar value={searchText} enterkeyhint="enter" onIonInput={e => e.target?.addEventListener('ionInput', searchResults)} showCancelButton="focus"></IonSearchbar>
            </IonRow>
            {
            // initData.data.map((data: any) => (
            //     data.id === props.user &&
            lists.length > 0 ?
                lists.map((books: any) => (
                    data.length > 1 ?
                        <IonRow key={books.book_id}>
                        <IonCol size-xs="10" size-sm="11" size-md="11" size-lg="11" size-xl="11">
                            <Collapsible className="book-lists" trigger={books.book} onOpening ={ () => { showChapter(books.book_id)}} onClosing  ={ () => { showChapter(books.book_id)}}>
                            <ChapterList chapters={books.chapters}/>
                            </Collapsible>
                        </IonCol>
                        <IonCol class="ion-text-center" size-xs="2" size-sm="1" size-md="1" size-lg="1" size-xl="1">
                            <IonButton color="books" size="small" shape="round"><p className="total-chapters">{books.total_chapters}</p></IonButton>
                        </IonCol>
                        </IonRow>
                        :
                        <IonRow key={books.book_id}>
                        <IonCol size-xs="10" size-sm="11" size-md="11" size-lg="11" size-xl="11">
                            <Collapsible open={true} className="book-lists" onOpening ={ () => { showChapter(books.book_id)}} trigger={books.book} onClosing={ () => { showChapter(books.book_id)}}>
                            <ChapterList chapters={books.chapters}/>
                            </Collapsible>
                        </IonCol>
                        <IonCol class="ion-text-center" size-xs="2" size-sm="1" size-md="1" size-lg="1" size-xl="1">
                            <IonButton color="books" size="small" shape="round"><p className="total-chapters">{books.total_chapters}</p></IonButton>
                        </IonCol>
                        </IonRow>
                ))
            :
                chapterLists.map((context: any) => (
                    <IonCard>
                        <IonCardContent>
                            <div className="chapter-content" dangerouslySetInnerHTML={createMarkup(context.content)}></div>
                        </IonCardContent>
                        <div className="tags-btn">
                        {
                            Object.entries(context.terms).map((item : any) => (
                                <IonButton onClick={()=> { setShowAlert3(true) }} size="small" color={randomColor()} shape="round">#{item[1]}</IonButton>
                            ))
                        }
                        </div>
                        <IonButton className="ion-margin ion-float-right" onClick={()=> { setCards(!showCards) }} key={context.uid} color="secondary" shape="round">Show</IonButton>
                    </IonCard>
                ))
            // )) 
            // !displayConstant ? : 
            //     (bookView && (chapterView || !chapterView)) 
            //     ?
            //         // searchBooksData.map((books:any) => (
            //         //     <h1>{books.name}</h1>
            //         // ))
            //         <h1>Book View</h1>
            //     :
            //         <h2>Chapter View</h2>
            }
            { showSpoken && <IonButton className="spoken-button" color="books" shape="round">Spoken Notes</IonButton> }
            { (showCards && showView === 'chapter') && 
                <IonRow>
                    <IonCol size="6">
                        <IonButton className="cards-button-left" color="books" shape="round">Speak</IonButton>
                    </IonCol>
                    <IonCol size="6">
                        <IonButton className="cards-button-right" color="books" shape="round">Cards</IonButton>
                    </IonCol>
                </IonRow>
            }
        </>
    )
}

export default BookList;