import React, { useState } from 'react';

import { IonHeader, IonModal,  IonButton, IonContent, IonToolbar, IonTitle, IonList, IonItem, IonAvatar, IonImg, IonLabel } from '@ionic/react';

const ModalTerm: React.FC<any> = (props) => {
  const [showModal, setShowModal] = useState(false);
  

  return (
            <IonList>
              <IonItem>
                {/* <IonAvatar slot="start">
                  <IonImg src="./avatar-gollum.jpg"/>
                </IonAvatar> */}
                <IonLabel>
                  <h2>Gollum</h2>
                  <p>Sneaky little hobbitses!</p>
                </IonLabel>
              </IonItem>
              <IonItem>
                {/* <IonAvatar slot="start">
                  <IonImg src="./avatar-frodo.jpg"/>
                </IonAvatar> */}
                <IonLabel>
                  <h2>Frodo</h2>
                  <p>Go back, Sam! I'm going to Mordor alone!</p>
                </IonLabel>
              </IonItem>
              <IonItem>
                {/* <IonAvatar slot="start">
                  <IonImg src="./avatar-samwise.jpg"/>
                </IonAvatar> */}
                <IonLabel>
                  <h2>Samwise</h2>
                  <p>What we need is a few good taters.</p>
                </IonLabel>
              </IonItem>
            </IonList>
  );
};

export default ModalTerm;