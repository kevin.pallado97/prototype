import React from 'react';

import '../pages/Home.css';

import { IonRow, IonCol, IonLabel, IonButton } from '@ionic/react';

const ChapterList: React.FC<any> = (props) => {

    const { chapters } = props;
    
    return(
        <>
        {chapters.map((segment:any, key:any) => (
            <IonRow key={segment.id} className="ion-margin-vertical">
                <IonCol className="chapter-lists" offset-xs="1" offset-sm="1" offset-md="1" offset-lg="1" offset-xl="1" size-xs="9" size-sm="10" size-md="10" size-lg="10" size-xl="10">
                    <IonLabel>{ segment.segment_chapter } : {segment.segment}</IonLabel>
                </IonCol>
                <IonCol size-xs="2" size-sm="1" size-md="1" size-lg="1" size-xl="1">
                    <IonButton color="books" size="small" shape="round"><p className="chapter-number">{ segment.segment_number }</p></IonButton>
                </IonCol>                
            </IonRow>
        ))}
        </>
    );
    
}

export default ChapterList;