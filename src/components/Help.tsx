import React, { useState } from 'react';

import { IonRow, IonItem, IonLabel, IonInput, IonTextarea, IonCol } from '@ionic/react';

const ModalTerm: React.FC<any> = (props) => {
    const [text, setText] = useState<string>();

    return (
        <div>
            <IonRow>
                <IonCol size="12">
                    <IonItem className="ion-margin">
                        <IonLabel position="stacked">Title</IonLabel>
                        <IonInput type="text"></IonInput>
                    </IonItem>
                </IonCol>
            </IonRow>
            <IonRow>
                <IonCol>
                    <IonItem className="ion-margin">
                        <IonLabel position="stacked">Comment</IonLabel>
                        <IonTextarea rows={6} cols={20} placeholder="Enter any notes here..." value={text} onIonChange={e => setText(e.detail.value!)}></IonTextarea>
                    </IonItem>
                </IonCol>
            </IonRow>
        </div>
    );
};

export default ModalTerm;